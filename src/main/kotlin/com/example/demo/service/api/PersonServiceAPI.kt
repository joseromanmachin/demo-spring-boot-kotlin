package com.example.demo.service.api

import com.example.demo.commons.GenericServiceAPI
import com.example.demo.model.Person


interface PersonServiceAPI : GenericServiceAPI<Person, Long> {
}