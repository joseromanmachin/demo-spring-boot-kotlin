package com.example.demo.service.impl

import com.example.demo.commons.GenericServiceImpl
import com.example.demo.model.Person
import com.example.demo.repositories.PersonaRepository
import com.example.demo.service.api.PersonServiceAPI
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service


@Service
class PersonaServiceImpl : GenericServiceImpl<Person, Long>(), PersonServiceAPI {

    @Autowired
    lateinit var personRepository: PersonaRepository



    override val dao: CrudRepository<Person, Long>
        get() = personRepository
}

