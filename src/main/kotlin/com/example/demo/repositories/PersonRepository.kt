package com.example.demo.repositories

import com.example.demo.model.Person
import org.apache.juli.logging.Log
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
interface PersonaRepository : CrudRepository<Person, Long> {
}