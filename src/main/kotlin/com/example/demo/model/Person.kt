package com.example.demo.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

@Entity(name = "person")
data class Person(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Long = 0,
        @Column
        val name:String = "",
        @Column(name="last_name")
        val last_name:String = "" ,
        @Column
        val address:String = "",
        @Column(name= "phone_number")
        val phone_number:String = ""
)
